const puppeteer   = require('puppeteer');
let currentDate   = new Date();
let date          = currentDate.getDate();
let month         = currentDate.getMonth();
let year          = currentDate.getFullYear();
let time          = currentDate.getTime();
let formattedTime = ( month + 1 ) + '-' + date + '-' + year + '-' + time;
let imageTitle    = 'cases-in-us';

( async () => {
    const browser = await puppeteer.launch({
        headless: true,
        devtools: true
    } );
    const page = await browser.newPage();
    await page.emulate({
        name: 'Desktop 600x1080',
        viewport: {
            width: 600,
            height: 1080
        },
        userAgent: 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.75 Safari/537.36'
    });
    try {
        await page.goto('https://www.cdc.gov/TemplatePackage/contrib/widgets/cdcMaps/build/index.html?chost=www.cdc.gov&cpath=/coronavirus/2019-ncov/cases-updates/cases-in-us.html&csearch=&chash=&ctitle=Cases%20in%20U.S.%20%7C%20CDC&wn=cdcMaps&wf=/TemplatePackage/contrib/widgets/cdcMaps/build/&wid=cdcMaps1&mMode=widget&mPage=&mChannel=&class=mb-3&host=www.cdc.gov&theme=theme-cyan&configUrl=/coronavirus/2019-ncov/cases-updates/map-cases-us-manual-desktop.json', {waitUntil: 'networkidle2'});
        await page.waitForSelector('section.geography-container');
        await page.addStyleTag( {
            content: 'section.geography-container{ padding:6px 0 6px }'
        } );
        // remove the legend
        await page.evaluate( () => {
            let legend = document.querySelector( 'aside' );
            legend.parentNode.removeChild( legend );
        } );
        const element = await page.$( 'section.geography-container' );
        // await element.screenshot( { path: 'snapshots/' + imageTitle + '-' + formattedTime + '.png' } ); // local testing
        await element.screenshot( { path: imageTitle + '.png' } );
        await browser.close();
    } catch (err)  {
        console.log('Error:', err);
        await browser.close();
    }
} )();